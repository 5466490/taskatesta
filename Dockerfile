FROM ubuntu:20.04

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -yq --no-install-recommends \
        apache2 \
        libapache2-mod-php \
        php \
        php-mbstring \
        php-xmlrpc \
        php-soap \
        php-gd \
        php-xml \
        php-cli \
        php-zip \
        php-bcmath \
        php-tokenizer \
        php-json \
        php-pear \
        php-mysql \
        nano \
        net-tools \
        wget \
        tar \
        make \
        sudo \
        composer \
        mysql-client \
    && apt-get clean && rm -rf /var/lib/apt/lists/*

RUN sed -i '/<Directory \/var\/www\/>/,/<\/Directory>/ s/AllowOverride None/AllowOverride All/' /etc/apache2/apache2.conf
COPY docker/apache/000-default.conf /etc/apache2/sites-available/000-default.conf

WORKDIR /var/www/html

ADD . /var/www/html

COPY docker/wait-for-it.sh /wait-for-it.sh
COPY docker/startup.sh /startup.sh
RUN chmod ugo+x /startup.sh /wait-for-it.sh

CMD /startup.sh