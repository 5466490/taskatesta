#!/bin/bash

chmod -Rv 777 /var/www/html/storage

composer install
cp .env.example .env

/wait-for-it.sh -t 0 db:3306 -- echo "MySQL is up"

php artisan migrate

a2enmod rewrite

apache2ctl -D FOREGROUND
